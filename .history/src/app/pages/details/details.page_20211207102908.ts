import { Component, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, IonContent, Platform, ToastController, ModalController, NavController } from '@ionic/angular';
import { GetApiService } from 'src/app/providers/services/get-api.service';
import { environment } from 'src/environments/environment.prod';
import { StorageService } from 'src/app/providers/providers';
import { SupportService } from 'src/app/providers/services/support.service';
import { PostApiService } from 'src/app/providers/services/post-api.service';
import EscPosEncoder from 'esc-pos-encoder-ionic';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ScanComponent } from 'src/app/components/scan/scan.component';
import { finalize } from 'rxjs/operators';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { HttpClient } from '@angular/common/http';

const IMAGE_DIR = 'stored-images';
 
interface LocalFile {
  name: string;
  path: string;
  data: string;
}

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})

export class DetailsPage {
  @ViewChild('content', { static: false }) content: IonContent;
  images: LocalFile[] = [];
  displayedImage: string = "";

  DEVICEID = environment.device_id;

  resultId : string;
  results: any;
  
  timestamp: string = "";
  isChecked: boolean;
  DS: number = 0;

  lang: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    public actionSheetController: ActionSheetController,
    public getapi: GetApiService,
    private support: SupportService,
    public alertCtrl: AlertController,
    private postapi: PostApiService,
    public loadingCtrl: LoadingController,
    private translate: TranslateService,
    private storageService: StorageService,
    private location: Location,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private plt: Platform,
    private http: HttpClient,
  ) {
    this.resultId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewWillEnter(){    
    this.storageService.getLang().then(lang => {
      if (!lang) {
        const localLang = this.translate.getBrowserLang();
        if(localLang != 'ko') {
          this.lang = 'en';
        } else {
        this.lang = this.translate.getBrowserLang();
        }
      } else {
        this.lang = lang;
      }
    });

    this.getapi.measureResult(this.resultId).subscribe((res) => {
      this.results = res;
      this.isChecked = this.results[0].checked;
      this.timestamp = `${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`;
      this.displayedImage = this.results[0].imgurl;
      this.countData();
    },
    (err) => {this.support.presentToast(err)}); 
  }

  scrollToBottom(): void {
    this.content.scrollToTop(300);
  }

  checkboxClick(e) {
    this.postapi.updateCheck(this.resultId, {bool: this.isChecked}).subscribe((res: any) => {
      this.countData();
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: this.translate.instant('MORE'),
      buttons: [{
        text: this.translate.instant('UPLOAD_SERVER'),
        icon: 'share-outline',
        handler: () => {
          //const data = [{"id": "10:81:F9:12:31:91", "datetime": "20200922181220246", "sensing": {"speed": 0.77, "latitude": 37.35, "longitude": 127.3, "altitude": 84.47, "distance": 0, "curingVolumeLitter": 1.0, "curingVolumeArea": 0.1}}];
          const datetime = this.results[0].datetime.replace('/-/g', '').replace('/:/g', '').replace('T', '').replace('Z', '').replace('.', '');
          const data = [{id: '10:81:F9:12:31:91', datetime: datetime, samNumber: this.results[0].memo, photo: 'http://app.dymeter.com:4000/images/'+this.results[0].imgurl, sensing: {aggregate: this.results[0].aggregate, slump: this.results[0].input_slump, airVolume: this.results[0].result_air, unitVolume: this.results[0].result_quantity}}];
          this.postapi.uploadIot(data).subscribe((res: any) => {
            if(res.result === 'success') {
              this.support.presentToast('서버에 성공적으로 업로드 되었습니다.');
            }
          });
        }
      },{
        text: this.translate.instant('PRINT'),
        icon: 'print',
        handler: () => {
          this.presentScan(this.resultId);
        }
      },{
        text: this.translate.instant('DELETE'),
        icon: 'trash',
        handler: () => {
          this.delete();
        },
      },{
        text: this.translate.instant('CLOSE'),
        icon: 'close',
        role: 'cancel',
      }]
    });
    await actionSheet.present();
  }

  async presentScan(id) {
    const modal = await this.modalCtrl.create({
      component: ScanComponent,
      componentProps: { 
        resultId: id,
        bType: 'YJ-300T'
      }
    });
    modal.onDidDismiss().then((modalData) => {
      console.log(modalData);
    });
    await modal.present();
  }
  
  async updateMemo() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('DETAILS.MEMO.UPDATE'),
      inputs: [
        {
          name: 'memo',
          value: this.results[0].memo,
          placeholder: this.translate.instant('DETAILS.MEMO.INPUT')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: data => {
            this.postapi.updateMemo(this.resultId, {memo: data.memo}).subscribe( _ => {
              this.getapi.measureResult(this.resultId).subscribe((res) => {
                this.results[0].memo = res[0].memo;
                this.countData();
              });
              this.support.presentToast(this.translate.instant('UPDATE_COMPLETE'));
            });
          }
        }
      ],
    });
    await alert.present();
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('DELETE'),
      message: this.translate.instant('DETAILS.DELETE.QUESTION'),
      buttons: [
        {
          text: this.translate.instant('CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('CONFIRM'),
          role: 'submit',
          handler: _ => {
            this.postapi.deleteResult(this.resultId).subscribe((res: any) => {
              this.location.back();
              this.support.presentToast(this.translate.instant('DETAILS.DELETE.DONE'));
            });
          }
        }
      ],
    });
    await alert.present();
  }

  countData() {
    const encoder = new EscPosEncoder();
    const escpos = encoder.initialize();

    if(this.isChecked) {  
      escpos
      .codepage('cp949')
      .line(`${this.results[0].cname}`)  //사업장
      .line(`${this.results[0].ct_name}`)  //계약자
      .line(`${this.results[0].sname}`)  //현장명
      .line(`${this.results[0].memo}`) //메모
      .line(`${(this.results[0].datetime).split(/[T.]+/)[0]} ${(this.results[0].datetime).split(/[T.]+/)[1]}`)  //날짜
      .line(`${this.results[0].mixnum}`) //배합번호
      .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
      .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
      .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
      .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
      .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
      .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
      .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
      .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
      .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
      .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
      .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
      .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
      .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
      .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
      .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
      .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
      .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
      .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
      .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
      .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
      .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
      .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
      .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
      .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
      .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
      .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
      .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
      .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
      .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
      .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
      .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
      .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
      .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
      .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
      
    } else {
      escpos
      .codepage('cp949')
      .line(`${this.results[0].cname}`)  //사업장
      .line(`${this.results[0].ct_name}`)  //계약자
      .line(`${this.results[0].sname}`)  //현장명
      .line(`${this.results[0].memo}`) //메모
      .line(`${(this.results[0].datetime).split(/[T.]+/)[0]}`)  //날짜
      .line(`${this.results[0].mixnum}`) //배합번호
      .line(NumberStr((this.results[0].w1_unit).toString(), 6) + NumberStr((this.results[0].w1_density).toString(), 7))
      .line(NumberStr((this.results[0].w2_unit).toString(), 6) + NumberStr((this.results[0].w2_density).toString(), 7))
      .line(NumberStr((this.results[0].w3_unit).toString(), 6) + NumberStr((this.results[0].w3_density).toString(), 7))
      .line(NumberStr((this.results[0].c1_unit).toString(), 6) + NumberStr((this.results[0].c1_density).toString(), 7))
      .line(NumberStr((this.results[0].c2_unit).toString(), 6) + NumberStr((this.results[0].c2_density).toString(), 7))
      .line(NumberStr((this.results[0].c3_unit).toString(), 6) + NumberStr((this.results[0].c3_density).toString(), 7))
      .line(NumberStr((this.results[0].mad1_unit).toString(), 6) + NumberStr((this.results[0].mad1_density).toString(), 7))
      .line(NumberStr((this.results[0].mad2_unit).toString(), 6) + NumberStr((this.results[0].mad2_density).toString(), 7))
      .line(NumberStr((this.results[0].mad3_unit).toString(), 6) + NumberStr((this.results[0].mad3_density).toString(), 7))
      .line(NumberStr((this.results[0].s1_unit).toString(), 6) + NumberStr((this.results[0].s1_density).toString(), 7))
      .line(NumberStr((this.results[0].s2_unit).toString(), 6) + NumberStr((this.results[0].s2_density).toString(), 7))
      .line(NumberStr((this.results[0].s3_unit).toString(), 6) + NumberStr((this.results[0].s3_density).toString(), 7))
      .line(NumberStr((this.results[0].g1_unit).toString(), 6) + NumberStr((this.results[0].g1_density).toString(), 7))
      .line(NumberStr((this.results[0].g2_unit).toString(), 6) + NumberStr((this.results[0].g2_density).toString(), 7))
      .line(NumberStr((this.results[0].g3_unit).toString(), 6) + NumberStr((this.results[0].g3_density).toString(), 7))
      .line(NumberStr((this.results[0].ad1_unit).toString(), 6) + NumberStr((this.results[0].ad1_density).toString(), 7))
      .line(NumberStr((this.results[0].ad2_unit).toString(), 6) + NumberStr((this.results[0].ad2_density).toString(), 7))
      .line(NumberStr((this.results[0].ad3_unit).toString(), 6) + NumberStr((this.results[0].ad3_density).toString(), 7))
      .line(NumberStr((this.results[0].air).toString(), 6)) // 목표공기량
      .line(NumberStr((this.results[0].aggregate).toString(), 6)) // 골재수정계수
      .line(NumberStr((this.results[0].wet).toString(), 6)) // 시멘트 습윤밀도
      .line(NumberStr((this.results[0].common_mass).toString(), 7)) // 용기질량
      .line(NumberStr((this.results[0].common_water).toString(), 7)) // 용기+물
      .line(NumberStr((this.results[0].common_volume).toString(), 7)) // 용기용적
      .line(NumberStr((this.results[0].input_slump).toString(), 7)) // 슬럼프
      .line(NumberStr((this.results[0].input_temp).toString(), 7)) // 온도
      .line(NumberStr((this.results[0].input_before).toString(), 7)) // 주수전질량
      .line(NumberStr((this.results[0].input_after).toString(), 7)) // 주수후질량
      .line(NumberStr((this.results[0].input_i_pressure).toString(), 7)) // 초기압력
      .line(NumberStr((this.results[0].input_e_pressure).toString(), 7)) // 평형압력
      .line(NumberStr(((this.results[0].mix_volume / 1000).toFixed(3)).toString(), 7)) // 이론용적
      .line(NumberStr((this.results[0].result_mass).toString(), 7)) // 단위용적질량
      .line(NumberStr((this.results[0].result_air).toString(), 7)) // 공기량
      .line(NumberStr((this.results[0].result_quantity).toString(), 7)) // 단위수량
      
    }
    this.DS = (escpos.encode()).length - 5;
  }

  async loadFiles() {
    this.images = [];
 
    const loading = await this.loadingCtrl.create({
      message: 'Loading data...',
    });
    await loading.present();
 
    Filesystem.readdir({
      path: IMAGE_DIR,
      directory: Directory.Data,
    }).then(result => {
      this.loadFileData(result.files);
    },
      async (err) => {
        // Folder does not yet exists!
        await Filesystem.mkdir({
          path: IMAGE_DIR,
          directory: Directory.Data,
        });
      }
    ).then(_ => {
      loading.dismiss();
    });
  }
 
  // Get the actual base64 data of an image
  // base on the name of the file
  async loadFileData(fileNames: string[]) {
    for (let f of fileNames) {
      const filePath = `${IMAGE_DIR}/${f}`;
 
      const readFile = await Filesystem.readFile({
        path: filePath,
        directory: Directory.Data,
      });
 
      this.images.push({
        name: f,
        path: filePath,
        data: `data:image/jpeg;base64,${readFile.data}`,
      });
    }
  }
 
  async selectImage() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Uri,
      source: CameraSource.Photos // Camera, Photos or Prompt!
    });

    if (image) {
        this.saveImage(image)
    }
  }

  // Create a new file from a capture image
  async saveImage(photo: Photo) {
    const base64Data = await this.readAsBase64(photo);

    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
        path: `${IMAGE_DIR}/${fileName}`,
        data: base64Data,
        directory: Directory.Data,
        recursive: true
    });

    // Reload the file list
    // Improve by only loading for the new image and unshifting array!
    this.loadFiles();
  }

  // https://ionicframework.com/docs/angular/your-first-app/3-saving-photos
  private async readAsBase64(photo: Photo) {
    if (this.plt.is('hybrid')) {
        const file = await Filesystem.readFile({
            path: photo.path
        });
        return file.data;
    }
    else {
        // Fetch the photo, read as a blob, then convert to base64 format
        const response = await fetch(photo.webPath);
        const blob = await response.blob();
 
        return await this.convertBlobToBase64(blob) as string;
    }
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });
 
  async startUpload(file: LocalFile) {
    const response = await fetch(file.data);
    const blob = await response.blob();
    const formData = new FormData();
    formData.append('file', blob, file.name);
    formData.append('id', this.resultId);
    this.uploadData(formData);
  }
 
  // Upload the formData to our API
  async uploadData(formData: FormData) {
      const loading = await this.loadingCtrl.create({
          message: '사진 업로드중...',
      });
      await loading.present();
  
      const url = 'http://app.dymeter.com:4000/api/upload-image';

      this.http.put(url, formData)
          .pipe(
              finalize(() => {
                  loading.dismiss();
              })
          )
          .subscribe(res => {
              if (res['success']) {
                  this.getapi.measureResult(this.resultId).subscribe((res) => {
                    this.displayedImage = res[0].imgurl;
                  });
                  this.support.presentToast('사진이 성공적으로 업로드 되었습니다.')
              } else {
                  this.support.presentToast('사진 업로드 실패')
              }
          });
  }
  
  async deleteImage(file: LocalFile) {
      await Filesystem.deleteFile({
          directory: Directory.Data,
          path: file.path
      });
      this.loadFiles();
      this.support.presentToast('File removed.');
  }
 
}

function NumberStr(tempstr: string, n: number) {
  let temp = new Array(), k=0;
  const i = tempstr.length;

  if (i > n) {
    console.log('Overflow Digit');
    return;
  }
  if(n == 6) {
    temp = [-1,-1,-1,-1,-1,-1];
  } else {
    temp = [-1,-1,-1,-1,-1,-1,-1];
  }

  for(let j=n-i; j < n; j++) {
    temp[j] = tempstr.charAt(k);
    k++;
  }
  let str = temp.join('');
  return str.replace(/-1/g,' ');
}
