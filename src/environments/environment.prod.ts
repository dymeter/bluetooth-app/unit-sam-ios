export const environment = {
  production: true,
  
  ioturl: 'http://15.164.138.60:8401/v2/admin/roadiot/sensor/signal',
  url: 'http://app.dymeter.com:4000/api',
  jwt_token: 'jwt_token',
  user_id: 'user_id',
  device_id: 'device_id',

  locker: '1234',

  uuid_service: 'FFF0',
  write_characteristic: 'FFF2',
  read_characteristic: 'FFF1',

};
