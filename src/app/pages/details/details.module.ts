import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailsPage } from './details.page';
import { TranslateModule } from '@ngx-translate/core';
import { ScanComponent } from 'src/app/components/scan/scan.component';
import { SharedMeasureModule } from 'src/app/providers/shared/shared-measure.module';

const routes: Routes = [
  {
    path: '',
    component: DetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedMeasureModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [DetailsPage],
  entryComponents: [
    ScanComponent
  ]
})
export class DetailsPageModule {}
