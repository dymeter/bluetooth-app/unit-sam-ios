import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: '/app/tabs/tab1', pathMatch: 'full' },
            {
              path: 'tabs',
              loadChildren: () => import('../tabs/tabs.module').then( m => m.TabsPageModule),
            },
            {
              path: 'complete-list', 
              loadChildren: () => import('../pages/complete-list/complete-list.module').then( m => m.CompleteListPageModule),
            },
            {
              path: 'time-series', 
              loadChildren: () => import('../pages/time-series/time-series.module').then( m => m.TimeSeriesPageModule),
            },
            {
              path: 'register', 
              loadChildren: () => import('../pages/register/register.module').then( m => m.RegisterPageModule),
            },
            { 
              path: 'details/:id',
              loadChildren: () => import('../pages/details/details.module').then( m => m.DetailsPageModule),
            },
            {
              path: 'details-print/:id',
              loadChildren: () => import('../pages/details-print/details-print.module').then( m => m.DetailsPrintPageModule),
            },
            { 
              path: 'bluetooth', 
              loadChildren: () => import('../pages/bluetooth/bluetooth.module').then( m => m.BluetoothPageModule),
            },
            { 
              path: 'bluetooths',
              loadChildren: () => import('../pages/bluetooths/bluetooths.module').then( m => m.BluetoothsPageModule),
            },
            {
              path: 'connect/:id',
              loadChildren: () => import('../pages/connect/connect.module').then( m => m.ConnectPageModule),
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
